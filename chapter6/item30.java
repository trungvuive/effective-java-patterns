// Use enum instead of int constant for fixed set of value represent a type

final class FruitShop {
    // we have 2 types of fruit, and each type has its own variant
    // The int enum pattern approach - deficient!
    public static final int APPLE_FUJI = 0;
    public static final int APPLE_PIPPIN = 1;
    public static final int APPLE_GRANNY_SMITH = 2;

    public static final int ORANGE_NAVEL = 0;
    public static final int ORANGE_TEMPLE = 1;
    public static final int ORANGE_BLOOD = 2;
    // even a string "APPLE_FUJI" would be inefficent as a compile-time

    public static void potentialMisuse() {
        System.out.println("Is an apple same with orange? " + APPLE_FUJI == ORANGE_NAVEL); // ?? nonsense
    }

    // clear and concise, provide type safety, and readability
    public enum Apple { FUJI, PIPPIN, GRANNY_SMITH };
    public enum Orange { NAVEL, TEMPLE, BLOOD };

    // enum are classes export one instance for each enumeration constant = public static final field, effectively immutable!

    // To associate data with enum constants, declare instance fields and write constructor that takes the data and stores it in the fields.
    // enum type of grape and price in VND
    public enum Grape {
        GREEN(68000),
        PURPLE(86000),
        RED(88000)
    };

    public static void goodUse() {
        
    }
}

final class Demonstration {
    public static void demonstrate() {
        FruitShop.potentialMisuse();
    }
}