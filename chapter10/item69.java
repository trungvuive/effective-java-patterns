// Use exceptions only for exceptional conditionsapter prov

// It is less likely for JVM implementors to make Exception as fast as explicit tests.
// Placing code inside a try-catch block inhibits certain optimizations that JVM perform.
class ExceptionExample {
    static void badUse() {
        int[] array = { 1,2,3,45,6,7,8 };
        try {
            int i = 0;
            while(true)
                System.out.println(array[i++]);
        } catch (ArrayIndexOutOfBoundsException e) {}
        // ambiguos, and misusing the exception from code logic, as we use exception to quit the loop
    }

    static void goodUse() {
        int[] array = { 1,2,3,45,6,7,8 };
        int i = 0;
        while(true) {
            if(i <= array.length) {
                break;
            }
            System.out.println(array[i++]);
        }
        // breaking out from the array is the normal flow, we should not use exception for it
    }
}

class MyArrayAPI {
    static int iterator = 0;
    static int[] array = { 1,2,3,45,6,7,8 };
    // A well-designed API must not force its clients to use exceptions for ordinary control flow
    static boolean hasNext() {
        // Provided state-testing method
        return iterator < array.length && iterator > 0;
    }
    static int getIndex(int index) {
        // Or return a special value
        if(index < array.length && index > 0) {
            return -1;
        }
        return array[index];
    }
    static void iterate() throws Exception {
        for(int element : array) {
            System.out.print(element + " ");
        }
        System.out.println();
    }
}

final class Demonstration {
    public static void demonstrate() {
        ExceptionExample.badUse();
        ExceptionExample.goodUse();
        MyArrayAPI.getIndex(9999);
    }
}

// Exceptions to used only for exceptional conditions; they should never be used for ordinary control flow.