// Use bounded wildcards to increase API flexibility

// Sometimes you need more flexibility than invariant typing parameterized type can provide
public class Stack<E> {
    public Stack();
    public void push(E e);
    public E pop();
    public boolean isEmpty();

    // We want to add a method that takes sequence of elements and pushes them all to stack

    // Here’s a first attempt: - deficient!
    public void pushAllNoWildCardType(Iterable<E> src) {
        for (E e : src) push(e);
    }

    // 2nd attempt: Wildcard type for parameter that serves as an E producer
    public void pushAllWithWildCardType(Iterable<? extends E> src) {
        for (E e : src) push(e);
    }

    // Now we want to write a popAll method to go with pushAll, pops each element off the stack and adds to given collection

    // 1st Attempt: popAll method without wildcard type - deficient!
    public void popAllNoWildCardType(Collection<E> dst) {
        while (!isEmpty()) dst.add(pop());
    }

    // 2nd attempt: Wildcard type for parameter that serves as an E consumer
    public void popAllWithWildCardType(Collection<? super E> dst) {
        while (!isEmpty()) dst.add(pop());
    }
}

// For maximum flexibility, use wildcard types on input parameters that represent producers, consumers
// PECS == producer-extends, consumer-super.