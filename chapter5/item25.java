// Prefer lists to array

// array type Sub[] is a subtype of Super[]
// But List<Sub> is neither subtype nor supertype of List<Super>

final class Demonstration {
    public static void demonstrate() {
        // Fails at runtime!
        Object[] objectArray = new Long[1];
        objectArray[0] = "I don't fit in"; // Throws ArrayStoreException

        // Won't compile!
        List<Object> ol = new ArrayList<Long>(); // Incompatible types
        ol.add("I don't fit in");

        // Why generic array creation is illegal - won't compile!
        List<String>[] stringLists = new List<String>[1];// (1)
        List<Integer> intList = Arrays.asList(42);// (2)
        Object[] objects = stringLists;// (3)
        objects[0] = intList;// (4)  <--- this line won't compile
        String s = stringLists[0].get(0);// (5)
    }
}

// Arrays provide runtime type safety but not compile-time type safety and vice versa for generics.
// Generally speaking, arrays and generics don’t mix well.