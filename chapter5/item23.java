// Don't use raw type in new code

// you lose type safety if you use a raw type like List, but not if you use a parameterized type like List<Object>

class Stamp {
    // class represent a stamp object
}

class Coin {
    // class represent a coin object
}

class StampCollection {
    // My stamp collection. Contains only Stamp instances
    // Now a raw collection type - don't do this!
    private static final Collection stampsUnsafe = new Collection();

    public void collectNewUnsafe() {
        // Erroneous insertion of coin into stamp collection, and no error produce
        stampsUnsafe.add(new Coin());
    }

    public void allStampsUnsafe() {
        // Now a raw iterator type - don't do this!
        for (Iterator i = stampsUnsafe.iterator(); i.hasNext(); ) {
            Stamp s = (Stamp) i.next(); // Throws ClassCastException
            // Do something with the stamp
        }
        console.log("All stamp in our collection is ok");
    }

    // Instead, use parameterized type
    public void collectNewSafe() {
        // Raise error immediately
        stampsSafe.add(new Coin());
    }

    public void allStampsSafe() {
        // Now a raw iterator type - don't do this!
        for (Iterator i = stampsSafe.iterator(); i.hasNext(); ) {
            Stamp s = (Stamp) i.next(); // Throws ClassCastException
            // Do something with the stamp
        }
        console.log("All stamp in our collection is ok");
    }
}

// Use of raw type for unknown element type - don't do this!
static int numElementsInCommon(Set s1, Set s2) {
    int result = 0;
    for (Object o1 : s1)
        if (s2.contains(o1))
            ++result;
    return result;
}

// Parameterized typeList<String>
// Actual type parameter String
// Generic typeList<E>I
// Formal type parameter E
// Unbounded wildcard typeList<?>\
// Raw type List
// Bounded type parameter<E extends Number>
// Recursive type bound<T extends Comparable<T>>
// Bounded wildcard typeList<? extends Number>Item 28
// Generic method static <E> List<E> asList(E[] a)
// Type token String.class