// Minimize the scope of local variables

// !! Declare variable where it first used, break the habit of declaring all at head of block
// --> Nlocal variable declaration should contain initial value, if cannot, leave it later

// String implements Comparable
class LocalVariablesExample {
    static void badArrayIterate() {
        System.out.println("Better code executed: ");
        int[] array = new int[]{ 1,2,3,4,5,6,7,8 };
        int iterator = 0;
        int element; // ? what is the inital value of element ?

        while(iterator < array.length) {
            element = array[iterator];
            System.out.print(element + " ");
            ++iterator;
        }
        System.out.println();
        // End of array now, but element still holding value? --> potential misuse
        System.out.println(element); // even raised variable maynot be initalized
    }

    static void betterArrayIterate() {
        System.out.println("Better code executed: ");
        int[] array = new int[]{ 1,2,3,4,5,6,7,8 };
        int iterator = 0; // ? what is the inital value of element ?

        while(iterator < array.length) {
            int element = array[iterator];
            System.out.print(element + " ");
            ++iterator;
        }
        System.out.println();
        System.out.println("Keep accessing element error cause compile error without executing");
        System.out.println("But the iterator still available, and we can modified it: " + (++iterator));
    }

    static void moreBetterArrayIterate() {
        // limiting variable scope to the exact region where they’re needed --> prefer for loops to while loops
        System.out.println("Good code executed: ");
        int[] array = new int[]{ 1,2,3,4,5,6,7,8 };

        for(int iterator=0, luckyNumber = expensiveComputation(); iterator<array.length; ++iterator) {
            int element = array[iterator];
            System.out.print(element + " ");
            ++iterator;

            if(iterator == array.length-1) {
                System.out.print(" ended with lucky number: " + luckyNumber);
            }
        }
        System.out.println();
        System.out.println("No more iterator to access now, cut down the use of memory and potential misuse");
    }

    private static int expensiveComputation() {
        System.out.println("An expensive computation has been executed only once with for loop!");
        return 68;
    }
}

final class Demonstration {
    public static void demonstrate() {
        LocalVariablesExample.badArrayIterate();
        LocalVariablesExample.betterArrayIterate();
        LocalVariablesExample.moreBetterArrayIterate();
    }
}

// general contract of the compareTo method is similar to that of equals