// Prefer for-each loops to traditional for loops

// !! Declare variable where it first used, break the habit of declaring all at head of block
// --> Nlocal variable declaration should contain initial value, if cannot, leave it later

// String implements Comparable

import java.util.Arrays;
import java.util.List;

class LoopExample {
    static void usualListIterate() {
        List<Integer> arraylist = List.of(1,2,3,4,5,6,7,8); // List.of for immutability
        // Not the best way to iterate over a collection!
        for (Iterator<Integer> iterator = arraylist.iterator(); iterator.hasNext(); ) {
            Integer element = iterator.next();
            System.out.print(element + " "); // Do something with element
        }

        System.out.println();
        System.out.println("This case, the usual way of iterate make the code less readability");
    }

    static void preferedListIterate() {
        List<Integer> arraylist = List.of(1,2,3,4,5,6,7,8); // List.of for immutability
        // Not the best way to iterate over a collection!
        for (Integer element : arraylist)
            System.out.print(element + " "); // Do something with element

        System.out.println();
        System.out.println("This prefered way of iterate just make the code concise, less memeory used, more readability");
    }
    
    static void buggyIterate() {
        // Can you spot the bug?
        enum Suit { CLUB, DIAMOND, HEART, SPADE };
        enum Rank { ACE, DEUCE, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING };

        List<Suit> suits = Arrays.asList(Suit.values());
        List<Rank> ranks = Arrays.asList(Rank.values());

        List<Pair<Suit, Rank>> deck = new ArrayList<>();

        for (Iterator<Suit> i = suits.iterator(); i.hasNext(); )
            for (Iterator<Rank> j = ranks.iterator(); j.hasNext(); )
                deck.add( new Pair<Suit, Rank>(i.next(), j.next()) );

        System.out.println();
        System.out.println("This case, the usual way of iterate make the code become buggy, unexpected in such a subtle way");
    }

    static void correctIterate() {
        // Can you spot the bug?
        enum Suit { CLUB, DIAMOND, HEART, SPADE };
        enum Rank { ACE, DEUCE, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING };

        List<Suit> suits = Arrays.asList(Suit.values());
        List<Rank> ranks = Arrays.asList(Rank.values());

        List<Pair<Suit, Rank>> deck = new ArrayList<>();

        for (Suit suit : suits)
            for (Rank rank : ranks)
                deck.add(new Card(suit, rank));

        System.out.println();
        System.out.println("This prefered way of just make the code concise, more readability and importantly, it is correct!");
    
}

final class Demonstration {
    public static void demonstrate() {
        LoopExample.usualListIterate();
        LoopExample.preferedListIterate();
        LoopExample.buggyIterate();
        LoopExample.correctIterate();
    }
}

// general contract of the compareTo method is similar to that of equals