// Minimize mutability

// Don’t provide methods modifying the object state(mutators).
// Ensure that the class can’t be extended carelessly or maliciously
// Make all fields final as possible, clearly expresses your intent in a manner that is enforced by the system
// Make all fields private. This prevents clients from obtaining access to mutable objects referred
// Ensure exclusive access to any mutable components

// a properly-written example
// Immutable complex number class
public final class Complex {
    private final double re;
    private final double im;
    public Complex(double re, double im) {
        this.re = re;
        this.im = im;
    }
    public double realPart() { return re; }
    public double imaginaryPart() { return im; }
    public Complex plus(Complex c) {
        return new Complex(re + c.re, im + c.im);
    }
    public Complex minus(Complex c) {
        return new Complex(re - c.re, im - c.im);
    }
    public Complex times(Complex c) {
        return new Complex(re * c.re - im * c.im, re * c.im + im * c.re);
    }
    public Complex dividedBy(Complex c) {
    double tmp = c.re * c.re + c.im * c.im;
        return new Complex((re * c.re + im * c.im) / tmp,
    (im * c.re - re * c.im) / tmp);
    }
    @Override public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Complex))
            return false;
        Complex c = (Complex) o;
        return Double.compare(c.re, re) == 0 && Double.compare(c.im, im) == 0;
    }
    @Override public int hashCode() {
        return 31 * Double.hashCode(re) + Double.hashCode(im);
    }
    @Override public String toString() {
        return "(" + re + " + " + im + "i)";
    }
}

final class Demonstration {
    public static final void demonstrate() {
        var num1 = new Complex(6.9, 9.6);
        var num2 = new Complex(6.8, 8.6);

        System.out.println(num1);
        System.out.println(num2);
        System.out.println(num1.equals(num2));

        System.out.println("Things work well!");
    }
}
// Immutable objects make great building blocks for other objects
// disadvantage of immutable classes is that require a separate object for each distinct value