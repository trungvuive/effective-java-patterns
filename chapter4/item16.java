// In degenerate classes, use accessor methods, not public fields

// Degenerate classes like this should not be public!
class BadPoint {
    public double x;
    public double y;
    public BadPoint(double x, double y) {
        this.x = x;
        this.y = y;
    }
}
// can’t change the representation without changing the API, you can’t enforce invariants, and you can’t take auxiliary action when a field is accessed

// Encapsulation of data by accessor methods and mutators
class GoodPoint {
    private double x;
    private double y;
    public GoodPoint(double x, double y) {
        this.x = x;
        this.y = y;
    }
    public double getX() { return x; }
    public double getY() { return y; }
    public void setX(double x) throws Exception {
        // imagine that we want our X point not exceed 9999
        if(x > 9999) {
            throw new Exception("X point not exceed 9999");
        }
        this.x = x;
    }
    public void setY(double y) { this.y = y; }
}

final class Demonstration {
    public static final void demonstrate() {
        var point1 = new BadPoint(6.9, 9.6);
        var point2 = new GoodPoint(6.8, 8.6);

        point1.x = 686868688; // unsafe, violate our rule
        try {
            point2.setX(686868688); // safe and handled
        } catch(Exception e) {
            System.out.println(e);
        }
    }
}