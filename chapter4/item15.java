// Minimize the accessibility of classes and members

// #1 make each class or member as inaccessible as possible

// that why we have unit test, to test hided details

class LeakyExample {
    // classes with public mutable fields are not generally thread-safe
    // nonzero-length array always mutable, so it is wrong for a class to have a public static final array field, or accessor returns such field
    public static final String[] LEAKY_ARRAY = { "Potential", "security", "hole", "!" };

    // even if you secure a private ARRAY, some IDE still generate below accessors return references to array fields\
    public static String[] LEAKY_ARRAY_accessor() {
        return LEAKY_ARRAY;
    }

    // that why we should have a dedicated public List of array copy, even to be unmodifiable
    public static final List<String> SAFE_LIST_COPY = Collections.unmodifiableList(Arrays.asList(LEAKY_ARRAY));
}

// In Java 9, there are 2 additional, implicit access levels introduced as part of the module system. 

final class Demonstration {
    public static final void demonstrate() {
        System.out.println("Before Modfied: " + LeakyExample.LEAKY_ARRAY[0] + LeakyExample.LEAKY_ARRAY[1] + LeakyExample.LEAKY_ARRAY[2]);
        LeakyExample.LEAKY_ARRAY[0] = "Real";
        System.err.println("Static object has been modified: " + LeakyExample.LEAKY_ARRAY[0] + LeakyExample.LEAKY_ARRAY[1] + LeakyExample.LEAKY_ARRAY[2]);
    }
}
// Generally,  if a class accessible outside its package, provide accessor methods
// But, somehow, if a class package-private or is a private nested class, there is nothing inherently wrong exposing its data fields