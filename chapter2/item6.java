// Remember to delete osolete object reference - GC is not magic

// Can you spot the "memory leak"?
class Stack {
    private Object[] elements;
    private int size = 0;
    private static final int DEFAULT_INITIAL_CAPACITY = 69;

    public Stack() {
        // stack init...
    }
    public void push(Object e) {
        // push code for stack...
    }

    public Object pop() {
        if (size == 0)
        throw new EmptyStackException();
        return elements[--size];
    }

    private void ensureCapacity() {
        // Ensure space for at least one more element,        
    }

    // whenever a class manages own memory, we should be alert for memory leaks.
    // try to avoid Unintentional Object Reference
    public Object popMemSafe() {
        if (size == 0)
        throw new EmptyStackException();
        var lastElement = elements[--size];
        elements[size] = null; // Eliminate obsolete reference
        return lastElement;
    }

    /* 
    *  But, nulling out object references should be the exception rather than the norm.
    *  best way to remove an obsolete reference is to let the variable that contained the reference fall out of scope
    */  
}

class MemSafeCache {
    private static WeakHashMap<Object, Object> hashmap = new WeakHashMap<>();

    public static void doSomethingWithCache() {
        System.out.println("Cache is another common source for MemLeak. Solution is to use GC-eligible DS like WeakHashMap...")
    }
}

