// Best way to implement Singleton Class??

// Way1 - public final field > performance(not in modern JVM)
class Singleton1 {
    public static final Singleton1 INSTANCE = new Singleton1();

    private Singleton1() {
        System.out.println("Constructor could be called only once!");
    }

    public void doSomethingWithINSTANCE() {
        System.out.println("Not to call constructor here!");
    }
}

// Way2 - static factory method > flexibility
public class Singleton2 {
    private static Singleton2 INSTANCE = new Singleton2();

    public Singleton1() { return INSTANCE; }

    public void doSomethingWithINSTANCE() {
        System.out.println("Not to call constructor here!");
    }
}

/* To serialize 2 ways above, further than "implement Serializable", we have to
   declare all instance fields "transient"
   add: private Object readResolve() { return INSTANCE; }
   in order to preserve singleton property
*/

// Way3 - with enum, probaly best way > even if serialization or reflection
public enum Singleton3 {
    Singleton3 INSTANCE;
    
    public void doSomethingWithINSTANCE() {
        System.out.println("Not to call constructor here!");
    }
}