// Noinstantiability with private constructor

// typical: Abstract Class ---> mislead user that only for inheritance
// avoid compiler adding default constructor

// Noninstantiable utility class
class UtilityClass {
    // Suppress default constructor for noninstantiability
    private UtilityClass() {
        throw new AssertionError();
    }
    // Remainder, should only static member
    public static utilityMethod() {
        // something likes Array.sort(), Collection.toArray()
    }
}