// Avoid creating unecessary object
// counterpoint to this is in item39.java 

final class Demonstration {
    private Demonstration() {}

    public static final void demonstrate() {
        System.out.println("See the java file for code example!!");
    }

    public static final void stringCreation() {
        String s1 = new String("stringette"); // DON'T DO THIS!, NUMBER OF OBJECT CAN BECOME LARGE!!
        String s2 = "stringette"; // SHOULD BE BETTTER!!
    }

    // --> prefer primitives to boxed primitives, and watch out for unintentional autoboxing.
    public static final void inefficentLoop() {
        // Hideously slow program!  
        Long sum = 0L;
        for (long i = 0; i < Integer.MAX_VALUE; ++i)
            sum += i;
        System.out.println(sum);
    }
    public static final void fastAndGoodLoop() {
        // Turned out that we only mistype ONE character above
        long sum = 0L;
        for (long i = 0; i < Integer.MAX_VALUE; ++i)
            sum += i;
        System.out.println(sum);
    }

    /* --> avoiding object creation by maintaining your own object pool. Maintaining your own object pools
        clutters code, increases footprint, harms performance. Modern JVM already got your back with lightweight objects. */
}