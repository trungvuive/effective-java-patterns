// Prefer lambdas to anonymous classes on Java 8+

class AnonymousClassExample {
    final static void sortWithAnonymousClassAsComparator() {
        List<String> list = List.of("this", "is", "a", "list", "of", "string");

        System.out.println("Before Sorting: ");
        for(var element : list) { System.out.print(element +  " "); }
        System.out.println();

        // Anonymous class instance as a function object - obsolete!
        Collections.sort(words, new Comparator<String>() {
            public int compare(String s1, String s2) {
                return Integer.compare(s1.length(), s2.length());
            }
        });

        System.out.println("After Sorting: ");
        for(var element : list) { System.out.print(element +  " "); }
        System.out.println();

        System.out.println("You may want to see classic implementation of Strategy Design Pattern and the Comparator Class!");
    }
}

class LambdasExample {
    final static void sortWithLambdasAsComparator() {
        List<String> list = List.of("this", "is", "a", "list", "of", "string");

        System.out.println("Lambdas are just syntax sugar of the anonymous class method - but much more concise, formalized");
        System.out.println("Before Sorting: ");
        for(var element : list) { System.out.print(element +  " "); }
        System.out.println();

        Collections.sort(words,
            (s1, s2) -> Integer.compare(s1.length(), s2.length())
        );
        // Omit the types of lambda parameters unless it makes your program clearer.

        // In fact, It can even more succinct if comparator construction method is used -> See the next Item
        Collections.sort(list, comparingInt(String::length));

        System.out.println("After Sorting: ");
        for(var element : list) { System.out.print(element +  " "); }
        System.out.println();


        System.out.println("Lambdas lack names and documentation; if a computation isn’t self-explanatory, or too long, don’t put it in a lambda.");
    }
    // you should rarely, if ever, serialize lambda(or an anonymous class instance)
}

final class Demonstration {
    public static void demonstrate() {
        AnonymousClassExample.sortWithAnonymousClassAsComparator();
        LambdaExample.sortWithLambdasAsComparator();
    }
}
// Summary, don’t use anonymous classes for function objects unless you have to create instances of that types