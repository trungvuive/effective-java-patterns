// Prefer method references to lambdas, whether it make sense

// See the merge() method for Map<> Interface here:
class MergeMethodExample {
    final static void mergeMapWithLambdas() {
        Map<Intger, Integer> map = ;

        System.out.println("Before Sorting: ");
        for(var element : list) { System.out.print(element +  " "); }
        System.out.println();

        // Anonymous class instance as a function object - obsolete!
        map.merge(key, 1, (count, incr) -> count + incr);

        System.out.println("After Sorting: ");
        for(var element : list) { System.out.print(element +  " "); }
        System.out.println();

        System.out.println("You can even get rid of parameters and get more succint by using method reference!");
    }

    final static void mergeMapWithMethodReference() {
        Map<Intger, Integer> map = ;

        System.out.println("You can easily find method reference of any predefined method from Java 8");
        System.out.println("Before Sorting: ");
        for(var element : list) { System.out.print(element +  " "); }
        System.out.println();

        map.merge(key, 1, Integer::sum);
        // nothing you can do with a method reference that you can’t also do with a lambda (with one exception—see JLS, 9.9-2)

        System.out.println("After Sorting: ");
        for(var element : list) { System.out.print(element +  " "); }
        System.out.println();


        System.out.println("In fact, your IDE will offer to replace a lambda with method reference wherever it can");
    }
}

final class Demonstration {
    public static void demonstrate() {
        MergeMethodExample.mergeMapWithLambdas();
        MergeMethodExample.mergeMapWithMethodReference();
    }
}
// Summary, where method references are shorter and clearer, use them; where they aren’t, stick with lambdas.