// Comsider Implementing Comparable

// bring us the Method comapreTo() 

// String implements Comparable
public class WordList {
    public WordList(String[] args) {
        Set<String> s = new TreeSet<String>();
        Collections.addAll(s, args);
        System.out.println(s);
    }
}

final class Demonstration {
    public static void demonstrate() {
        // By implementing Comparable, a class indicates that its instances have a natural ordering
        var wordList = new WordList( { "abc", "def", "ghi" } );

        // easy to search, compute extreme values, and maintain automatically sorted collections
        Arrays.sort( { new BigDecimal("1.00"), new BigDecimal("2.03"), new BigDecimal("1.00") } );
    }
}

// general contract of the compareTo method is similar to that of equals