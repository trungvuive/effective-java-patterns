// Remember to override toString()

// providing a good toString implementation makes your class much more pleasant to use

final class PhoneNumber {
    private final short areaCode, prefix, lineNumber;

    public PhoneNumber(int areaCode, int prefix, int lineNumber) {
        rangeCheck(areaCode, 999, "area code");
        rangeCheck(prefix, 999, "prefix");
        rangeCheck(lineNumber, 9999, "line number");

        this.areaCode = (short) areaCode;
        this.prefix = (short) prefix;
        this.lineNumber = (short) lineNumber;
    }
    private static void rangeCheck(int arg, int max, String name) {
        if (arg < 0 || arg > max)
        throw new IllegalArgumentException(name +": " + arg);
    }
    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof PhoneNumber))
            return false;
        PhoneNumber pn = (PhoneNumber)o;
        return pn.lineNumber == lineNumber && pn.prefix == prefix && pn.areaCode == areaCode;
    }
    // Broken - no toString() method overrided!

    // Override, but still Broken. Can you correct??
    @Override public String toString() { return " who knows?"; }
    // Remainder omitted
}

final class Demonstration {
    public static void demonstrate() {
        var numberOfSomeone = new PhoneNumber(707, 867, 5309);
        var numberOfOtherOne = new PhoneNumber(868, 969, 5368);
        Map<PhoneNumber, String> phonebook = new HashMap<PhoneNumber, String>();
        phonebook.put(numberOfSomeone, "Barrack Obama");
        phonebook.put(numberOfOtherOne, "Vladimir Putin");

        for(Map.Entry<PhoneNumber, String> entry : phonebook.entrySet()) {
            System.out.println("Phone number of " + entry.getValue() + " is: " + entry.getKey());
        }
    }
}