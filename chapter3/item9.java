// Remember to override hashCode() when override equals()

// If not, we would mess up with HashMap, HashSet, HashTable...
final class PhoneNumber {
    private final short areaCode, prefix, lineNumber;

    public PhoneNumber(int areaCode, int prefix, int lineNumber) {
        rangeCheck(areaCode, 999, "area code");
        rangeCheck(prefix, 999, "prefix");
        rangeCheck(lineNumber, 9999, "line number");

        this.areaCode = (short) areaCode;
        this.prefix = (short) prefix;
        this.lineNumber = (short) lineNumber;
    }
    private static void rangeCheck(int arg, int max, String name) {
        if (arg < 0 || arg > max)
        throw new IllegalArgumentException(name +": " + arg);
    }
    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof PhoneNumber))
            return false;
        PhoneNumber pn = (PhoneNumber)o;
        return pn.lineNumber == lineNumber && pn.prefix == prefix && pn.areaCode == areaCode;
    }
    // Broken - no hashCode method overrided!

    // The worst possible legal hash function - it turns your hashmap int LinkedList!!!
    @Override public int hashCode() { return 42; }
    // Remainder omitted
}

final class Demonstration {
    public static void demonstrate() {
        var numberOfSomeone = new PhoneNumber(707, 867, 5309);
        Map<PhoneNumber, String> phonebook = new HashMap<PhoneNumber, String>();
        phonebook.put(numberOfSomeone, "Barrack Obama");

        System.out.println("This should output a name, but: " + phonebook.get(numberOfSomeone));
    }
}