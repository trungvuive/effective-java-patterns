// Obey general contract when overriding equals()

// Broken - violates symmetry!
public final class CaseInsensitiveString {
    private final String s;
    public CaseInsensitiveString(String s) {
        if (s == null)
            throw new NullPointerException();
        this.s = s;
    }
    // Broken - violates symmetry!
    public boolean equals1(Object o) {
        if (o instanceof CaseInsensitiveString)
            return s.equalsIgnoreCase(((CaseInsensitiveString) o).s);
        if (o instanceof String) // One-way interoperability!
            return s.equalsIgnoreCase((String) o);
        return false;
    }

    // No way to extend an instantiable class and add a value component while preserving the equals contract
    // Broken - violates Liskov substitution principle
    public boolean equals2(Object o) {
        if (o == null || o.getClass() != getClass())
            return false;
        return this.equals(o);
    }

    // FIXED BELOW
    @Override
    public boolean equals(Object o) {
        return o instanceof CaseInsensitiveString && ((CaseInsensitiveString) o).s.equalsIgnoreCase(s);
    }
}

final class Demonstration {
    public static void demonstrate() {
        CaseInsensitiveString x = new CaseInsensitiveString("Polish");
        String y = "polish";
        String z = new String("polish");

        // REFLEXIVE
        if( !x.equals(x) ) {
            System.out.println("No Reflexitivity using equals()");
        }
        // SYMMETRIC
        if( x.equals(y) != y.equals(x) ) {
            System.out.println("No Symmetry using equals()");
        }
        // TRANSITIVITY
        if( x.equals(y) && y.equals(z) && !z.equals(x) ) {
            System.out.println("No Transitivity using equals()");
        }
        // CONSISTENCY
        if( x != null && x.equals(null) ) {
            System.out.println("No Consistency using equals()");
        }

        // Once you’ve violated the equals contract, you simply don’t know how other objects will behave when confronted with your object.
    }
}